            ;========================================================================
;  Assembler key Pad handler
;========================================================================
;

;--- register definitions for ATMEGA32.
.include "c:\VMLab\include\M32DEF.INC"   ;**warning** you will ned to change this line
.include "c:\VMLab\project\si1143.inc"
;------ Reset and interrupt vectors ------------------------------
;
.ORG  0x00
START:
   rjmp  RESET  ; Reset vector
   reti
   reti
   reti
   reti
   reti
   reti
   reti
   reti
   reti
   reti
   reti
   reti
   reti
   reti

.DEF  TEMP   = R19
.DEF	TEMP2	 = R20
.DEF	CmdTemp = R21

;-------CONSTANTS-----
.EQU 	PRESCALER_MASK = 0xF8

.dseg
;-------VARIABLES-----
DISPLAY_DATA:	.Byte		1
CurrSampleVal:	.Byte		2
CurrSampleVal2: .Byte	2
.cseg

;------ Device initialization -------------------------------------

.ORG  0x10
;------ initializations.
RESET:
	clr	R0				  			; use as zero value
   ldi   TEMP, LOW(RAMEND)    ; Init stack pointer
   out   SPL,  TEMP    			; SPL defined in M32DEF.INC
   ldi 	TEMP, HIGH(RAMEND)
   out	SPH,  TEMP

	LDI	TEMP,0x34
	out	TWBR,TEMP
	
	CALL	DELAY
	CALL  DELAY
	CALL	DELAY
	CALL	DELAY
	CALL  DELAY
	CALL	DELAY
	CALL	DELAY
	CALL  DELAY
	CALL	DELAY
	CALL	DELAY
	CALL  DELAY
	CALL	DELAY
	CALL	DELAY
	CALL  DELAY
	CALL	DELAY
	CALL	DELAY
	CALL  DELAY
	CALL	DELAY
		
init:	
   CALL	InitDevice
   CALL	StartMeasurement
   CALL	ReadSamples
	
   ;Init BaseValues
   LDI	ZH,high(SampleVal+(REG_PS1_DATA0-REG_ALS_VIS_DATA0))
   LDI	ZL,low(SampleVal+(REG_PS1_DATA0-REG_ALS_VIS_DATA0))
   LDI	YH,high(BaselineVal)
	LDI	YL,low(BaselineVal)
   LD		TEMP,Z+
   ST		Y+,TEMP
   LD		TEMP,Z+
   ST		Y+,TEMP
   LD		TEMP,Z+
   ST		Y+,TEMP
   LD		TEMP,Z+
   ST		Y+,TEMP
   LD		TEMP,Z+
   ST		Y+,TEMP
   LD		TEMP,Z+
   ST		Y+,TEMP


MainLoop:

	CALL	StartMeasurement
		
	Call	ReadSamples
	
	 ;---------------------TEST-----------------------------
	LDS	TEMP,SampleVal+(REG_PS1_DATA0-REG_ALS_VIS_DATA0)+5
	STS	DISPLAY_DATA,TEMP
	CALL	DISPLAY
	;------------------------------------------------------
	
  	rjmp 	MainLoop

rjmp END

;------------------------------------------------------------------
InitDevice:

	LDI	TEMP, CMD_RESET
	STS   CommandValue, TEMP
	CALL	SendCommand
	
	;Send Hardware Key
	LDI	TEMP,REG_HW_KEY	
	STS	I2C_REG,TEMP
	LDI	TEMP,HW_KEY_VAL0
	STS	I2C_VAL,TEMP	
	CALL  I2CWriteReg
	
	;Set LED to default
	LDI	TEMP,REG_PS_LED21
	STS	I2C_REG,TEMP
	LDI	TEMP,(DEFAULT_LED_CURRENT<<4) + DEFAULT_LED_CURRENT
	STS	I2C_VAL,TEMP
   CALL	I2CWriteReg

   LDI	TEMP,REG_PS_LED3
   STS	I2C_REG,TEMP
	LDI	TEMP,DEFAULT_LED_CURRENT
	STS	I2C_VAL,TEMP
	CALL  I2CWriteReg
	
	;Channel List
	LDI	TEMP,REG_PARAM_WR
   STS	I2C_REG,TEMP
   LDI	TEMP,ALS_IR_TASK + ALS_VIS_TASK + PS1_TASK + PS2_TASK + PS3_TASK
   STS	I2C_VAL,TEMP
   CALL	I2CWriteReg
  	LDI	TEMP,CMD_PARAM_SET + (PARAM_CH_LIST & $1F)
   STS	CommandValue,TEMP
  	CALL	SendCommand

	;Disable ALL interrupts
	CLR	R0
	LDI	TEMP,REG_IRQ_CFG
	STS	I2C_REG,TEMP	
	STS	I2C_VAL,R0
	CALL	I2CWriteReg
	LDI	TEMP,REG_IRQ_ENABLE
	STS	I2C_REG,TEMP	
	STS	I2C_VAL,R0
	CALL	I2CWriteReg
	LDI	TEMP,REG_IRQ_MODE1
	STS	I2C_REG,TEMP	
	STS	I2C_VAL,R0
	CALL	I2CWriteReg	
	LDI	TEMP,REG_IRQ_MODE2
	STS	I2C_REG,TEMP	
	STS	I2C_VAL,R0
	CALL	I2CWriteReg
ret	

StartMeasurement:
	LDI	TEMP,CMD_PSALS_FORCE
	STS	CommandValue,TEMP
	CALL	SendCommand
ret

ReadSamples:
	LDI	ZH,high(SampleVal)
 	LDI	ZL,low(SampleVal)
   LDI	TEMP2,REG_ALS_VIS_DATA0
  	CALL	GetSample       ;get REG_ALS_VIS_DATA0
  	CALL	GetSample       ;get REG_ALS_VIS_DATA1
  	CALL	GetSample       ;get REG_ALS_IR_DATA0
  	CALL	GetSample       ;get REG_ALS_IR_DATA1
  	CALL	GetSample       ;get REG_PS1_DATA0
   CALL	GetSample       ;get REG_PS1_DATA1
   CALL	GetSample       ;get REG_PS2_DATA0
   CALL	GetSample       ;get REG_PS2_DATA1
   CALL	GetSample       ;get REG_PS3_DATA0
   CALL	GetSample       ;get REG_PS3_DATA1
	LDS	TEMP,SampleCnt
	INC	TEMP
	STS	SampleCnt,TEMP
	LDI	ZH,high(SampleVal+(REG_PS1_DATA0-REG_ALS_VIS_DATA0))
	LDI	ZL,low(SampleVal+(REG_PS1_DATA0-REG_ALS_VIS_DATA0))   ;Move Z pointer to start of sample data
	LDI	YH,high(BaselineVal)
	LDI	YL,low(BaselineVal)              ;Move Y pointer to start of baselines samples

	LDI	R22,6
CheckWithBase:
	CLR	R0
	CPSE	R22,R0
	rjmp	cont
	ret
cont:
	DEC	R22
	LD		TEMP,Z
	LD    TEMP2,Y+
	CP		TEMP,TEMP2
	BRSH	greater
	LDI	TEMP,0
	ST		Z+,TEMP
	RJMP	CheckWithBase
greater:
	SUB	TEMP,TEMP2
	ST		Z+,TEMP
	rjmp	CheckWithBase	
ret	
	
SendCommand:
	LDI	TEMP,REG_RESPONSE        ;I2C_REG=REG_RESPONSE
	STS	I2C_REG,TEMP
	CALL  I2CReadReg
	LDS	TEMP,I2C_VAL
	CLR	R0
	CPSE  TEMP,R0					;if(I2C_VAL!=0)
	rjmp	loop
	rjmp	endloop
;SPAM NOP's until reg is 0	
loop:
	LDI	TEMP,REG_COMMAND       ;I2C_REG =REG_COMMAND
	STS	I2C_REG,TEMP
	LDI	TEMP,CMD_NOP
	STS	I2C_VAL,TEMP 				;I2C_VAL = CMD_NOP
	CALL  I2CWriteReg
	LDI	TEMP,REG_RESPONSE       ;I2C_REG =REG_RESPONSE
	STS	I2C_REG,TEMP
	CALL	I2CReadReg
	LDS	TEMP,I2C_VAL
	CLR	R0	
   CPSE	TEMP,R0                 ;if(I2C_VAL ==0)return
 rjmp  loop						
endLoop:
	LDI	TEMP,REG_COMMAND       ;I2C_REG =REG_COMMAND
	STS	I2C_REG,TEMP
	LDS	TEMP,CommandValue       ;I2C_VAL =CommandValue
	STS	I2C_VAL,TEMP
   CALL	I2CWriteReg
   LDS	TEMP,CommandValue
   CPI	TEMP,CMD_RESET				;IF(CommandValue == CMD_RESET)
   BRNE	notEqual
   ret                           ;ret
notEqual:
	LDI	TEMP,REG_RESPONSE
   STS	I2C_REG,TEMP
 	CALL	I2CReadReg
 	LDS	TEMP,I2C_VAL
 	CLR	R0
 	CPSE	TEMP,R0
 	ret									;val != 0 return
  	rjmp	endLoop
ret  	

I2CReadReg:
	CALL 	I2CStart
	LDI	TEMP,I2C_ADDR_WRITE
	STS	I2C_DATA,TEMP
	CALL	I2CWrite
	LDS	TEMP,I2C_REG        			;I2C_DATA=I2C_REG
	STS	I2C_DATA,TEMP
	CALL	I2CWrite
	CALL	I2CStop
	CALL	I2CStart
	LDI	TEMP,I2C_ADDR_READ
	STS	I2C_Data,TEMP
	CALL	I2CWrite
	CALL	I2CRead
	;CALL	I2CNack
	CALL	I2CStop
	;I2C_VAL=I2C_DATA
	LDS	TEMP,I2C_DATA
	STS	I2C_VAL,TEMP
ret
	
I2CWriteReg:
	CALL  I2CStart
	LDI	TEMP,I2C_ADDR_WRITE
	STS	I2C_DATA,TEMP	
	CALL  I2CWrite
	LDS	TEMP,I2C_REG       ;I2C_DATA=I2C_REG
	STS	I2C_DATA,TEMP
	CALL	I2CWrite
	LDS	TEMP,I2C_VAL       ;I2C_DATA=I2C_VAL
	STS	I2C_DATA,TEMP
	CALL	I2CWrite
	CALL	I2CStop
ret
	
I2CWrite:
	PUSH TEMP	        ;save temp	
	LDS TEMP,I2C_DATA       ;load data
	OUT TWDR,TEMP
	LDI TEMP,(1<<TWINT)|(1<<TWEN)   ;init twi
   OUT TWCR,TEMP
   CALL twiWait            ;check send
	POP TEMP
	ret
	
I2CRead:
	PUSH TEMP
	LDI TEMP,I2C_ADDR_READ
	OUT TWDR, TEMP
	
	;---Initiate sending of slave addr
	LDI TEMP,(1<<TWINT)|(1<<TWEN)
	OUT TWCR, TEMP
	call twiWait
	
	IN TEMP,TWDR
	STS I2C_DATA,TEMP
	call twiWait	
	
	;----Tell slave to send data byte
	;LDI TEMP,(1<<TWINT)|(1<<TWEA)|(1<<TWEN)
	LDI	TEMP,(1<<TWEN)
	OUT	TWCR, TEMP
	call	twiWait	

	POP TEMP
	ret
			
I2CStart:
	ldi  TEMP, (1<<TWINT)|(1<<TWSTA)|(1<<TWEN)
	OUT  TWCR,TEMP
	CALL twiWait	
	ret

I2CStop:
	LDI  TEMP,(1<<TWINT)|(1<<TWEN)|(1<<TWSTO)
	OUT  TWCR,TEMP
	ret
	
I2CNack:
	ldi r16, (1<<TWINT)|(1<<TWEN)|(1<<TWEA) ; Enable aknowledge
   out TWCR, r16
	ret	

twiWait:
	IN   TEMP,TWCR
   SBRS TEMP,TWINT	
   rjmp twiWait
ret

Display:
	LDI	TEMP,0xFF
	OUT	DDRB,TEMP
	LDS	TEMP,DISPLAY_DATA
	OUT	PORTB,TEMP
ret

;Z Pointer needs to be set to beginning of memory location
;i.e SampleVal, Also beginning sensor register needs to be set in
;temp2
GetSample:
   STS	I2C_REG,TEMP2
   INC 	TEMP2
   CALL	I2CReadReg
   LDS	TEMP,I2C_VAL
	ST		Z+,TEMP     		;SampleVal(i)
  ret

Delay:
         PUSH TEMP		; save R16 and 17 as we're going to use them
         PUSH TEMP2       ; as loop counters
         CLR TEMP        ; init inner counter
         CLR TEMP2        ; and outer counter
loop1:   DEC TEMP         ; counts down from 0 to FF to 0
			CPSE TEMP, R0    ; equal to zero?
			RJMP loop1			 ; if not, do it again
			CLR TEMP			 ; reinit inner counter
loop2:   DEC TEMP2
         CPSE TEMP2, R0    ; is it zero yet?
         RJMP loop1			 ; back to inner counter
         POP TEMP2
         POP TEMP
RET

;---------------------TEST-----------------------------
	LDI	TEMP,0x01
	STS	DISPLAY_DATA,TEMP
	CALL	DISPLAY
	rjmp END
;------------------------------------------------------

  ;--------TEST------
  	LDI	TEMP,REG_COMMAND       ;I2C_REG =REG_COMMAND
   STS	I2C_REG,TEMP
	LDS	TEMP,CommandValue       ;I2C_VAL =CommandValue
	STS	I2C_VAL,TEMP
   CALL	I2CWriteReg
   LDI	TEMP,REG_RESPONSE
  	STS	I2C_REG,TEMP
  	CALL	I2CReadReg
  	LDS	TEMP,I2C_VAL
  	STS	DISPLAY_DATA,TEMP
  	CALL	DISPLAY
  	RJMP END  	
  	;-------------


 END:
    	
.exit




